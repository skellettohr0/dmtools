from random import *
from player import *

'''Wurfmethode, die einen beliebigen Würfelwurf emuliert durch pseudorandom Zahlengeneration.
Der Übergebene Parameter ist die  Art des Würfels. Bei einem nicht sechsseitigen Würfel ändert diese Fkt. die
Hintergrundfarbe des Buttons bei kritischem Erfolg(1)/Misserfolg(4)'''


def roll(dice):
    temp = text = randint(1, dice)
    if dice == 6:
        Screen.d6_result.configure(text="  " + str(temp))
    else:
        if temp < 10:
            Screen.d20_result.configure(text="  " + str(temp))
        else:
            Screen.d20_result.configure(text=temp)
        if temp == 4:
            Screen.d20_button.configure(bg="red")
        elif temp == 1:
            Screen.d20_button.configure(bg="green")
        else:
            Screen.d20_button.configure(bg="light grey")


'''Funktion zeigt für einen gewählten Spieler einen gewählten Stat (drop_down_stat) an'''


def show_stat():
    Screen.stat.configure(text=Screen.active_player.player_stats[Screen.stats.get()])


'''Funktion zeigt bei gewähltem Spieler die Anzahl der Ability-Punkte zur gewählten Fähigkeit an'''


def show_ability_points_func():
    Screen.show_ability_points_value.configure(text=Screen.active_player.player_abilities[Screen.ability_player.get()][3])


'''Funktion ermöglicht das Ändern eines der Stats im jeweiligen dictonary
um einen eingegebenen Werte (drop_down_alter_stat)'''


def add_stat_p1():
    Screen.active_player.player_stats[Screen.update.get()] = Screen.active_player.player_stats[Screen.update.get()] + int(Screen.alter_stat_value.get())
    if Screen.update.get() == "HP":
        update_stats()
    else:
        update_permanent()
    Screen.stat.configure(text=Screen.active_player.player_stats[Screen.stats.get()])


'''Funktion ändert den die ausgewählten Ability-Punkte um den spezifizierten Wert'''


def add_ability():
    Screen.active_player.player_abilities[Screen.ability_player.get()][3] = Screen.active_player.player_abilities[Screen.ability_player.get()][
                                                                  3] + int(Screen.alter_ability_value.get())
    show_ability_points_func()


'''Funktion die den aktiven Spieler übergeben bekommt und diesen global setzt'''


def set_player(player):
    Screen.active_player = player


'''Funktion, die die HP anzeige aktualisiert, falls der Wert geändert worden ist (wird von add_stat gecallt)'''


def update_stats():
    colors = ["black", "gold", "orange", "red"]
    old_hp_status = Screen.active_player.hp_status
    Screen.active_player.hp_label.configure(
        text="HP: " + str(Screen.active_player.player_stats["HP"]) + "/" + str(Screen.active_player.base_hp))
    if Screen.active_player.player_stats["HP"] <= Screen.active_player.base_hp / 4:
        Screen.active_player.hp_status = 3
    elif Screen.active_player.player_stats["HP"] <= Screen.active_player.base_hp / 2:
        Screen.active_player.hp_status = 2
    elif Screen.active_player.player_stats["HP"] <= (3 * Screen.active_player.base_hp) / 4:
        Screen.active_player.hp_status = 1
    elif Screen.active_player.player_stats["HP"] >= (3 * Screen.active_player.base_hp) / 4:
        Screen.active_player.hp_status = 0

    Screen.active_player.player_stats["Power"] += old_hp_status - Screen.active_player.hp_status
    Screen.active_player.player_stats["Speed"] += old_hp_status - Screen.active_player.hp_status
    Screen.active_player.player_stats["Precision"] += old_hp_status - Screen.active_player.hp_status
    Screen.active_player.hp_label.configure(fg=colors[Screen.active_player.hp_status])
    update_permanent()


'''Funktion, die die permanenten Stats aktualisiert, falls diese geändert werden (wird von add_stat gecallt)'''


def update_permanent():
    for each in Screen.active_player.positions:
        Screen.active_player.new_labels[each].configure(text=str(each) + ": " + str(Screen.active_player.player_stats[each]))


'''Funktion, die den letzten hinzugefügten permanenten Stat wieder entfernt'''


def remove_last_permanent():
    Screen.active_player.new_labels[Screen.active_player.positions[Screen.active_player.perm_stat_pos - 1]].destroy()
    Screen.active_player.positions.pop(Screen.active_player.perm_stat_pos - 1)
    Screen.active_player.perm_stat_pos -= 1


'''Funktion, die die Anzeige der permanenten Stats regelt und verwaltet'''


def show_permanent():

    if Screen.stats.get() in Screen.active_player.positions:
        pass
    else:
        Screen.active_player.positions.append(str(Screen.stats.get()))
        lb1 = Label(Screen.active_player.player_frame, font=("Arial", 14),
                    text=str(Screen.stats.get()) + ": " + str(Screen.active_player.player_stats[Screen.stats.get()]))
        lb1.grid(column=0, row=Screen.active_player.current_row)
        Screen.active_player.new_labels[Screen.active_player.positions[Screen.active_player.perm_stat_pos]] = lb1
        Screen.active_player.current_row += 1
        Screen.active_player.perm_stat_pos += 1


'''Funktion, die den aktuell ausgewählten Stat für alle Spieler anzeigt'''


def show_perm_all():
    for each in all_players:
        set_player(each)
        show_permanent()


'''Funktion, die für alle Spieler den ausgewählten Stat wieder entfernt.'''


def remove_perm_all():
    for each in all_players:
        set_player(each)
        remove_last_permanent()


'''Aus Platzgründen wird im roll Tab der Spieler über ein drop_down Menü ausgewählt. Diese Funktion konvertiert die
Auswahl in den global aktiv gesetzten Spieler'''


def set_player_in_roll():
    for player_ in all_players:
        if Screen.player.get() == player_.name:
            Screen.active_player = player_
            break


'''Diese Funktion zeigt während eines Wurfes im roll Tab auf eine Fähigkeit sowohl die Namen, als auch Werte
der betroffenen Fähigkeit an.'''


def show_ability_values():
    points_shown = False
    set_player_in_roll()
    for i in range(3):
        Screen.ability_names[i].configure(text=Screen.active_player.player_abilities[Screen.ability.get()][i])
    if not points_shown:
        Screen.ability_names[3].configure(text="Ability points")
        points_shown = True
    for i in range(3):
        Screen.ability_values[i].configure(text=Screen.active_player.player_stats[Screen.active_player.player_abilities[Screen.ability.get()][i]])
    Screen.ability_values[3].configure(text=Screen.active_player.player_abilities[Screen.ability.get()][3])


'''Diese Funktion evaluiert den Wurf. Sollte ein kritischer Erfolg oder Misserfolg geworfen worden sein, wird die Zahl
grün/rot angezeigt. Sollte ein Wurf erfolgreich sein (auch mit Ausgleichspunkten), so wird er blau angezeigt. Bei einem
Nisserfolg Magenta.'''


def check_throw(val, lbl, current, points):
    temp = Screen.active_player.player_stats[Screen.active_player.player_abilities[Screen.ability.get()][current]]
    if val == 4:
        lbl.configure(fg="red")
    elif val == 1:
        lbl.configure(fg="green")
    elif val <= temp:
        lbl.configure(fg="blue")
    elif val > temp:
        if val <= temp + points:
            lbl.configure(fg="blue")
            return points + temp - val
        else:
            lbl.configure(fg="magenta")
            return 0
    return points


'''Diese Funktion managed die Fähigkeitswürfe.'''


def roll_on_ability():
    set_player_in_roll()
    print(Screen.active_player)
    ab_points = Screen.active_player.player_abilities[Screen.ability.get()][3]
    throws = []
    for i in range(3):
        throws.append(randint(1, 20))
    show_ability_values()
    for i in range(3):
        Screen.abilities_thrown[i].configure(text=str(throws[i]))
        ab_points = check_throw(throws[i], Screen.abilities_thrown[i], i, ab_points)
    Screen.abilities_thrown[3].configure(text=str(ab_points))


'''Diese Funktion managed das Erstellen der Zugreihenfolge im combat Tab.'''


def initiate_turn_order():
    for each in all_players:
        Screen.all_comb_dict.update({each.name: each.player_stats["Initiative"]})
    Screen.all_comb_dict = sort_turn_order()
    Screen.labels_created = True
    create_labels_turn_order(list(Screen.all_comb_dict.keys()))


'''Diese Funktion sorgt für das Erstmalige erstellen der Zugreihenfolge. Und kann bei genauem Nachdenken ziemlich
sicher einfach mit der nachfolgenden Funktion zusammengelegt werden...'''


def create_labels_turn_order(quick_arr):
    Screen.turns[0].configure(text=quick_arr[0], relief=RAISED, fg="green")
    for i in range(1,len(quick_arr)):
        Screen.turns[i].configure(text=quick_arr[i])


'''Diese Funktion zykelt einmal durch das Dictonary mit der Zugreihenfolge durch und updatet die Zugreihenfolge.'''


def next_turn():
    arr = list(Screen.all_comb_dict.items())
    Screen.all_comb_dict = dict(arr[1:]+[arr[0]])
    quick_arr = list(Screen.all_comb_dict.keys())
    create_labels_turn_order(quick_arr)


'''Diese Funktion erstellt ein Dictionary für den aktiven Gegner.'''


def create_enemy():
    Screen.enemy = {"Name": Screen.enemy_name_entry.get(), "HP": int(Screen.enemy_hp_entry.get()), "Initiative": int(Screen.enemy_init_entry.get())}
    Screen.all_comb_dict.update({Screen.enemy["Name"]: Screen.enemy["Initiative"]})
    update_enemy_status()


'''Diese Funktion sortiert das Dictionary nach der Zugreihenfolge (Initiativewert) in absteigender Reihenfolge.'''


def sort_turn_order():
    return dict(sorted(Screen.all_comb_dict.items(), key=lambda item: item[1], reverse=True))


"Diese Funktion zeigt den erstellten Gegner im combat Tab an."


def update_enemy_status():
    Screen.enemy_status_name.configure(text=Screen.enemy["Name"])
    Screen.enemy_status_hp.configure(text="HP: " + str(Screen.enemy["HP"]))


'''Diese Funktion verändert bei Schaden die Lebenspunkte des Gegners.'''


def alter_enemy_hp():
    Screen.enemy["HP"] = Screen.enemy["HP"] + int(Screen.alter_enemy_hp_entry.get())
    update_enemy_status()


'''Diese Klasse enthält alle Tkinter Displayelemente'''


class Screen:
    main_window = Tk()

    main_window.title("CDMT Version 1.1")
    tab_control = ttk.Notebook(main_window)

    tab1 = ttk.Frame(tab_control)
    tab2 = ttk.Frame(tab_control)
    tab3 = ttk.Frame(tab_control)

    '''Globale Variabeln der einzelnen drop_down Menüs:'''
    stats = StringVar()
    update = StringVar()
    ability = StringVar()
    ability_player = StringVar()
    player = StringVar()

    active_player = None
    all_comb_dict = {}
    labels_created = False
    current_turn_row = 0
    enemy = {}

    tab_control.add(tab1, text='roll')
    tab_control.add(tab2, text='players')
    tab_control.add(tab3, text="combat")
    tab_control.pack(expand=1, fill='both')
    for player_ in all_players:
        player_.create_frame(tab2)

    sidebar_frame = LabelFrame(tab2, padx=5, pady=5)
    sidebar_frame.grid(padx=50, pady=50, column=2, row=0, rowspan=2, sticky="n")

    sidebar_abilities_frame = LabelFrame(tab2, padx=5, pady=5)
    sidebar_abilities_frame.grid(pady=50, column=3, row=0, sticky="n")

    sidebar_save = LabelFrame(tab2, padx=5, pady=5)
    sidebar_save.grid(pady=50, column=3, row=1, sticky="n")

    roll_1_frame = LabelFrame(tab1, padx=5, pady=5)
    roll_1_frame.grid(padx=50, pady=50, column=0, row=0)

    roll_on_frame = LabelFrame(tab1, padx=5, pady=5)
    roll_on_frame.grid(padx=50, pady=50, column=1, row=0)

    roll_legend = LabelFrame(tab1, padx=5, pady=5)
    roll_legend.grid(padx=25, pady=25, column=2, row=0)

    d6_button = Button(roll_1_frame, text="D6", bg="light grey", padx=21, pady=5, command=lambda: roll(6))
    d6_button.grid(column=0, row=0)

    d6_result = Label(roll_1_frame, text="", font=("Arial", 18))
    d6_result.grid(column=1, row=0)

    blank_line = Label(roll_1_frame, text="")
    blank_line.grid(columnspan=2, row=1)

    d20_button = Button(roll_1_frame, text="D20", bg="light grey", padx=18, pady=5, command=lambda: roll(20))
    d20_button.grid(column=0, row=2)

    d20_result = Label(roll_1_frame, text="", font=("Arial", 18))
    d20_result.grid(column=1, row=2)

    save_button = Button(sidebar_save, text="Save player data", font=("Arial", 18), command=save_players)
    save_button.grid(column=0, row=0)

    values = list(all_players[0].player_stats.keys())
    abilities = list(all_players[0].player_abilities.keys())

    drop_down_stats = OptionMenu(sidebar_frame, stats, *values)
    drop_down_stats.grid(column=0, row=1)

    drop_down_alter_stat = OptionMenu(sidebar_frame, update, *values)
    drop_down_alter_stat.grid(column=0, row=6)

    drop_down_alter_abilities = OptionMenu(sidebar_abilities_frame, ability_player, *abilities)
    drop_down_alter_abilities.grid(column=0, row=0)

    drop_down_abilities = OptionMenu(roll_on_frame, ability, *abilities)
    drop_down_abilities.grid(column=0, row=0)

    player_names = [player_.name for player_ in all_players]
    drop_down_players = OptionMenu(roll_on_frame, player, *player_names)
    drop_down_players.grid(column=1, row=0)

    show_roll_on = Button(roll_on_frame, text="Show values", padx=11, pady=5, command=show_ability_values)
    show_roll_on.grid(column=2, row=0)

    roll_on_button = Button(roll_on_frame, text="Roll dice", padx=15, pady=50, command=roll_on_ability)
    roll_on_button.grid(column=4, rowspan=4)

    ability_names = []
    for i in range(4):
        ability_names.append(Label(roll_on_frame, text="", font=("Arial", 18), ))
        ability_names[i].grid(column=i, row=1)

    ability_values = []
    for i in range(4):
        ability_values.append(Label(roll_on_frame, text="", font=("Arial", 18), ))
        ability_values[i].grid(column=i, row=2)

    abilities_thrown = []
    for i in range(4):
        abilities_thrown.append(Label(roll_on_frame, text="", font=("Arial", 18), ))
        abilities_thrown[i].grid(column=i, row=3)

    legend = {}
    colors = ['blue', 'magenta', 'green', 'red']
    description = {'blue': 'check passed', 'magenta': 'check failed', 'green': 'critical success',
                   'red': 'critical failure'}
    for i in range(4):
        legend[colors[i]] = Label(roll_legend, text=(colors[i] + " = " + description[colors[i]]), font=("Arial", 12))
        legend[colors[i]].grid(column=0, row=i)

    stat_view = Button(sidebar_frame, text="Show stat", padx=30, pady=5, command=show_stat)
    stat_view.grid(column=1, row=1)

    alter_stat_value = Entry(sidebar_frame)
    alter_stat_value.grid(column=1, row=6)

    alter_stat = Button(sidebar_frame, text="Alter stat", command=add_stat_p1)
    alter_stat.grid(column=2, row=6)

    stat = Label(sidebar_frame, text="", font=("Arial", 18))
    stat.grid(column=2, row=1)

    show_stat_permanent = Button(sidebar_frame, text="Show permanently", padx=6, pady=5, command=show_permanent)
    show_stat_permanent.grid(column=1, row=2)

    remove_stat_permanent = Button(sidebar_frame, text="Remove last", padx=24, pady=5, command=remove_last_permanent)
    remove_stat_permanent.grid(column=1, row=3)

    add_all = Button(sidebar_frame, text="Show for all", padx=25, pady=5, command=show_perm_all)
    add_all.grid(column=1, row=4)

    remove_all = Button(sidebar_frame, text="Remove last for all", padx=7, pady=5, command=remove_perm_all)
    remove_all.grid(column=1, row=5)

    show_ability_points = Button(sidebar_abilities_frame, text="Show ability", padx=21, pady=5,
                                 command=show_ability_points_func)
    show_ability_points.grid(column=1, row=0)

    show_ability_points_value = Label(sidebar_abilities_frame, text="", font=("Arial", 18))
    show_ability_points_value.grid(column=2, row=0)

    alter_ability_value = Entry(sidebar_abilities_frame)
    alter_ability_value.grid(column=1, row=1)

    alter_ability_button = Button(sidebar_abilities_frame, text="Alter ability", command=add_ability)
    alter_ability_button.grid(column=2, row=1)

    '''Platzierung der Objekte für den 3. Tab.'''

    turn_order_frame = LabelFrame(tab3, padx=5, pady=5)
    turn_order_frame.grid(padx=50, pady=50, column=0, row=0)

    create_enemy_frame = LabelFrame(tab3, padx=5, pady=5, text="Create Enemy")
    create_enemy_frame.grid(padx=50, pady=50, column=1, row=0)

    show_turns_frame = LabelFrame(tab3, padx=5, pady=5)
    show_turns_frame.grid(padx=50, pady=50, column=0, row=1)

    enemy_status_frame = LabelFrame(tab3, padx=5, pady=5)
    enemy_status_frame.grid(column=1, row=1)

    turns = []
    for i in range(5):
        turns.append(Label(show_turns_frame, text="", font=("Arial", 18)))
        turns[i].grid(column=0, row=i)

    create_turn_order_button = Button(turn_order_frame, text="Create turn order", padx=6, pady=5,
                                      command=initiate_turn_order)
    create_turn_order_button.grid(column=0, row=0)

    next_turn_button = Button(turn_order_frame, text="Next turn", padx=26, pady=5, command=next_turn)
    next_turn_button.grid(column=0, row=1)

    enemy_name_label = Label(create_enemy_frame, text="Enemy name:", font=("Arial", 12))
    enemy_name_label.grid(column=0, row=0)
    enemy_name_entry = Entry(create_enemy_frame)
    enemy_name_entry.grid(column=0, row=1)

    enemy_hp_label = Label(create_enemy_frame, text="Enemy HP:", font=("Arial", 12))
    enemy_hp_label.grid(column=0, row=2)
    enemy_hp_entry = Entry(create_enemy_frame)
    enemy_hp_entry.grid(column=0, row=3)

    enemy_init_label = Label(create_enemy_frame, text="Enemy initiative:", font=("Arial", 12))
    enemy_init_label.grid(column=0, row=4)
    enemy_init_entry = Entry(create_enemy_frame)
    enemy_init_entry.grid(column=0, row=5)

    create_enemy_button = Button(create_enemy_frame, text="Create", command=create_enemy)
    create_enemy_button.grid(column=1, row=0)

    enemy_status_name = Label(enemy_status_frame, text="", font=("Arial", 18))
    enemy_status_name.grid(column=0, row=0)

    enemy_status_hp = Label(enemy_status_frame, text="", font=("Arial", 18))
    enemy_status_hp.grid(column=0, row=1)

    blank_in_status = Label(enemy_status_frame, text="", font=("Arial", 18))
    blank_in_status.grid(column=0, row=2)

    alter_enemy_hp_entry = Entry(enemy_status_frame)
    alter_enemy_hp_entry.grid(column=0, row=3)

    alter_enemy_hp_button = Button(enemy_status_frame, text="Alter HP", command=alter_enemy_hp)
    alter_enemy_hp_button.grid(column=1, row=3)