from tkinter import ttk
from tkinter import *
import json
import framework


class Player:
    ID = 0

    def __init__(self, name, values, abilities):
        self.name = name
        self.player_stats = values
        self.player_stats_base = self.player_stats
        self.player_abilities = abilities
        self.base_hp = self.player_stats['HP']
        self.hp_status = 0
        self.current_row = 2
        self.perm_stat_pos = 0
        self.new_labels = {}
        self.positions = []
        self.ID = Player.ID
        Player.ID += 1
        self.p = None
        self.player_frame = None
        self.hp_label = None

    def create_frame(self, tab):
        self.player_frame = LabelFrame(tab, padx=5, pady=5)
        self.player_frame.grid(padx=50, pady=50, column=self.check_column(), row=self.check_row())

        self.p = Button(self.player_frame, text=self.name, font=("Arial", 18), command=lambda: framework.set_player(self))
        self.p.grid(column=0, row=0)

        self.hp_label = Label(self.player_frame, font=("Arial", 14),
                              text="HP: " + str(self.player_stats["HP"]) + "/" + str(self.base_hp))
        self.hp_label.grid(column=0, row=1)

    def check_row(self):
        return self.ID // 2

    def check_column(self):
        return self.ID % 2


def save_players():
    players_ = []
    for player_ in all_players:
        data = {'name' : player_.name, 'values' : player_.player_stats, 'abilities': player_.player_abilities}
        players_.append(data)
    with open('players.json', 'w') as json_file:
        json.dump(players_, json_file, indent = 4)


def get_players():
    players_ = []
    with open('players.json') as json_file:
        data = json.load(json_file)
        for player_ in data:
            print(player_['name'])
            players_.append(Player(player_['name'], player_['values'], player_['abilities']))
    return players_


all_players = get_players()

